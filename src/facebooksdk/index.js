/* eslint-disable */
export const fbSdkInit = (Vue, fbAppId) => {
  window.fbAsyncInit = () => {
    FB.init({
      appId      : fbAppId,
      cookie     : true,
      xfbml      : true,
      version    : 'v4.0'
    });
      
    // FB.AppEvents.logPageView();
    
    /*
      Get login status when app starts
    */
    FB.getLoginStatus(function(response) {
        statusChangeCallback(response);
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));

  function statusChangeCallback(response) {
      if(response.status === 'connected') {
          console.log('Logged in and authenticated');
      } else {
          console.log('Not authenticated');
      }
  }
}