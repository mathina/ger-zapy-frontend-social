import Vue from 'vue';
import VueI18n from 'vue-i18n'
import store from '../store'

import en from './en-Us.json'
import th from './th-TH.json'
import vi from './vi-VI.json'

Vue.use(VueI18n)

const locale = store.getters.lang

const messages = {   
    en,
    th,
    vi
}

const i18n = new VueI18n({
    locale,
    messages
})

export default i18n