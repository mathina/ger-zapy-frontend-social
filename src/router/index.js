import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/login/index'
import Preloader from '@/components/login/loading'
//CHECKIN COMPONENTS
import Splash from '@/components/checkin/splash'
import CheckIn from '@/components/checkin/index'
import CheckinConfirm from '@/components/checkin/confirm'
//CHECKOUT COMPONENTS
import CheckOut from '@/components/checkout/index'
import CheckOutConfirm from '@/components/checkout/confirm'
//REGISTRATION COMPONENTS
import Registration from '@/components/registration/index'
import Register from '@/components/registration/register'
import Profile from '@/components/registration/profile'
import Confirm from '@/components/registration/confirm'
import Done from '@/components/registration/done'
//LUCKYDRAW COMPONENTS
import LuckyDraw from '@/components/luckydraw/index'
import StartDraw from '@/components/luckydraw/start'
import LuckyDrawTapYourWristband from '@/components/luckydraw/tapYourWristband'
import CompleteDraw from '@/components/luckydraw/complete'
import LuckyDone from '@/components/luckydraw/done'
//BOOTH COMPONENTS
import Booth from '@/components/booth/index'
import BoothScan from '@/components/booth/scan'
import BoothDone from '@/components/booth/done'
//REDEEM COMPONENTS
import Redeem from '@/components/redeem/index'
import RedeemItems from '@/components/redeem/redeemItems'
import RedeemConfirm from '@/components/redeem/redeemConfirm'
import RedeemComplete from '@/components/redeem/redeemComplete'
import RedeemTapYourWristband from '@/components/redeem/tapYourWristband'
//Q&A COMPONENTS
import QA from '@/components/QA/index'
import type1 from '@/components/QA/type1'
import type2 from '@/components/QA/type2'
import type3 from '@/components/QA/type3'
import proceed from '@/components/QA/proceed'
//VOTING COMPONENTS
import Voting from '@/components/voting/index'
import Vote from '@/components/voting/vote'

import emailTemplate from '@/components/emailTemplate'
import ScanQR from '@/components/shared/QR/readQR'
Vue.use(Router)

export default new Router({
    routes: [{
            path: '/',
            name: 'Login',
            component: Login
        },
        {
            path: '/scanhere',
            name: 'ScanHere',
            component: Splash
        },
        {
            path: '/preloader',
            name: 'Loader',
            component: Preloader
        },
        {
            path: '/checkin',
            name: 'Checkin',
            component: CheckIn
        },
        {
            path: '/checkinconfirm',
            name: 'Checkinconfirm',
            component: CheckinConfirm
        },
        {
            path: '/checkout',
            name: 'Checkout',
            component: CheckOut
        },
        {
            path: '/checkoutconfirm',
            name: 'CheckOutConfirm',
            component: CheckOutConfirm
        },
        {
            path: '/registration',
            name: 'Registration',
            component: Registration
        },
        {
            path: '/register',
            name: 'Register',
            component: Register
        },
        {
            path: '/profile',
            name: 'Profile',
            component: Profile
        },
        {
            path: '/confirm',
            name: 'Confirm',
            component: Confirm
        },
        {
            path: '/done',
            name: 'RegDone',
            component: Done
        },
        {
            path: '/luckydraw',
            name: 'LuckyDraw',
            component: LuckyDraw
        }, {
            path: '/luckydraw/start',
            name: 'StartDraw',
            component: StartDraw
        }, {
            path: '/luckydraw/tapyourwrisband',
            name: 'LuckytapYourWristband',
            component: LuckyDrawTapYourWristband
        }, {
            path: '/luckydraw/complete',
            name: 'CompleteDraw',
            component: CompleteDraw
        },
        {
            path: '/luckydraw/done',
            name: 'LuckyDone',
            component: LuckyDone
        },
        {
            path: '/booth',
            name: 'Booth',
            component: Booth
        },
        {
            path: '/booth/scan',
            name: 'BoothScan',
            component: BoothScan
        },
        {
            path: '/booth/done',
            name: 'BoothDone',
            component: BoothDone
        },
        {
            path: '/redeem',
            name: 'Redeem',
            component: Redeem
        },
        {
            path: '/redeem/items',
            name: 'redeemItems',
            component: RedeemItems
        },
        {
            path: '/redeem/confirm',
            name: 'redeemConfirm',
            component: RedeemConfirm
        },
        {
            path: '/redeem/tapyourwristband',
            name: 'RedeemtapYourWristband',
            component: RedeemTapYourWristband
        },
        {
            path: '/redeem/complete',
            name: 'redeemComplete',
            component: RedeemComplete
        },
        {
            path: '/qa',
            name: 'qa',
            component: QA
        },
        {
            path: '/qa/type1',
            name: 'QA-Type-1',
            component: type1
        },
        {
            path: '/qa/type2',
            name: 'QA-Type-2',
            component: type2
        },
        {
            path: '/qa/type3',
            name: 'QA-Type-3',
            component: type3
        },
        {
            path: '/qa/proceed',
            name: 'proceed',
            component: proceed
        },       
        {
            path: '/email/template',
            name: 'emailTemplate',
            component: emailTemplate
        },
        {
            path: '/scanqr',
            name: 'ScanQR',
            component: ScanQR
        },
        {
            path: '/voting',
            name: 'Voting',
            component: Voting
        },
        {
            path: '/vote',
            name: 'Vote',
            component: Vote
        }

    ]
})