import Vue from 'vue'
import Router from 'vue-router'
import CheckOut from '@/components/checkout/index'
import CheckOutConfirm from '@/components/checkout/confirm'
import ScanQR from '@/components/shared/QR/readQR'

Vue.use(Router)

export default new Router({
    routes: [{
            path: '/',
            name: 'Checkout',
            component: CheckOut
        },
        {
            path: '/checkoutconfirm',
            name: 'CheckOutConfirm',
            component: CheckOutConfirm
        },
        {
            path: '/scanqr',
            name: 'ScanQR',
            component: ScanQR
        }
    ]
})