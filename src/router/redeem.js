import Vue from 'vue'
import Router from 'vue-router'
import Redeem from '@/components/redeem/index'
import RedeemItems from '@/components/redeem/redeemItems'
import RedeemConfirm from '@/components/redeem/redeemConfirm'
import RedeemComplete from '@/components/redeem/redeemComplete'
import TapYourWristband from '@/components/redeem/tapYourWristband'
import ScanQR from '@/components/shared/QR/readQR'

Vue.use(Router)

export default new Router({
    routes: [{
            path: '/',
            name: 'Redeem',
            component: Redeem
        },
        {
            path: '/redeem/items',
            name: 'redeemItems',
            component: RedeemItems
        },
        {
            path: '/redeem/confirm',
            name: 'redeemConfirm',
            component: RedeemConfirm
        },
        {
            path: '/redeem/tapyourwristband',
            name: 'tapYourWristband',
            component: TapYourWristband
        },
        {
            path: '/redeem/complete',
            name: 'redeemComplete',
            component: RedeemComplete
        },
        {
            path: '/scanqr',
            name: 'ScanQR',
            component: ScanQR
        }
    ]
})