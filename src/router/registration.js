import Vue from 'vue'
import Router from 'vue-router'
import Registration from '@/components/registration/index'
import Register from '@/components/registration/register'
import Profile from '@/components/registration/profile'
import Confirm from '@/components/registration/confirm'
import Done from '@/components/registration/done'
import ScanQR from '@/components/shared/QR/readQR'

Vue.use(Router)

export default new Router({
    routes: [{
            path: '/',
            name: 'Registration',
            component: Registration
        },
        {
            path: '/register',
            name: 'Register',
            component: Register
        },
        {
            path: '/profile',
            name: 'Profile',
            component: Profile
        },
        {
            path: '/confirm',
            name: 'Confirm',
            component: Confirm
        },
        {
            path: '/done',
            name: 'Done',
            component: Done
        },
        {
            path: '/scanqr',
            name: 'ScanQR',
            component: ScanQR
        }
    ]
})