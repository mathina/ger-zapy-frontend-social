import Vue from 'vue'
import Router from 'vue-router'
import Booth from '@/components/booth/index'
import Scan from '@/components/booth/scan'
import Done from '@/components/booth/done'
import ScanQR from '@/components/shared/QR/readQR'

Vue.use(Router)

export default new Router({
    routes: [{
            path: '/',
            name: 'Booth',
            component: Booth
        },
        {
            path: '/booth/scan',
            name: 'Scan',
            component: Scan
        },
        {
            path: '/booth/done',
            name: 'Done',
            component: Done
        },
        {
            path: '/scanqr',
            name: 'ScanQR',
            component: ScanQR
        }

    ]
})