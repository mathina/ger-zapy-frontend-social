import Vue from 'vue'
import Router from 'vue-router'
import Index from '@/components/QA/index'
import type1 from '@/components/QA/type1'
import type2 from '@/components/QA/type2'
import type3 from '@/components/QA/type3'
import proceed from '@/components/QA/proceed'


Vue.use(Router)

export default new Router({
    routes: [{
            path: '/',
            name: 'qa',
            component: Index
        },       
        {
            path: '/qa/type1',
            name: 'QA-Type-1',
            component: type1
        },
        {
            path: '/qa/type2',
            name: 'QA-Type-2',
            component: type2
        },
        {
            path: '/qa/type3',
            name: 'QA-Type-3',
            component: type3
        },
        {
            path: '/qa/proceed',
            name: 'proceed',
            component: proceed
        }           

    ]
})