import Vue from 'vue'
import Router from 'vue-router'
import Pos from '@/components/pos/index'
import Order from '@/components/pos/order'
import OrderConfirm from '@/components/pos/orderConfirm'
import OrderComplete from '@/components/pos/orderComplete'

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            name: 'Pos',
            component: Pos
        },        
        {
            path: '/order',
            name: 'order',
            component: Order
        },
        {
            path: '/order/confirm',
            name: 'orderConfirm',
            component: OrderConfirm
        },
        {
            path: '/order/complete',
            name: 'orderComplete',
            component: OrderComplete
        }
    ]
})