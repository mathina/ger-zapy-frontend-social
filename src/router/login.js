import Vue from 'vue'
import Router from 'vue-router'
import preloader from '@/components/shared/preloader'
import Registration from '@/components/registration/index'
import Register from '@/components/registration/register'
import Confirm from '@/components/registration/confirm'
import CheckinConfirm from '@/components/checkin/confirm'

Vue.use(Router)

export default new Router({
    routes: [{
            path: '/',
            name: 'Loader',
            component: preloader
        },
        {
            path: '/registration',
            name: 'Registration',
            component: Registration
        },
        {
            path: '/register',
            name: 'Register',
            component: Register
        },
        {
            path: '/confirm',
            name: 'Confirm',
            component: Confirm
        },
        {
            path: '/checkinconfirm',
            name: 'Checkinconfirm',
            component: CheckinConfirm
        }
    ]
})