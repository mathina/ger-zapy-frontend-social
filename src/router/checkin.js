import Vue from 'vue'
import Router from 'vue-router'
// import Splash from '@/components/checkin/splash'
import CheckIn from '@/components/checkin/index'
import CheckinConfirm from '@/components/checkin/confirm'
import ScanQR from '@/components/shared/QR/readQR'

Vue.use(Router)

export default new Router({
    routes: [
        // {
        //     path: '/',
        //     name: 'Splash',
        //     component: Splash
        // },
        {
            path: '/',
            name: 'Checkin',
            component: CheckIn
        },
        {
            path: '/checkinconfirm',
            name: 'Checkinconfirm',
            component: CheckinConfirm
        },
        {
            path: '/scanqr',
            name: 'ScanQR',
            component: ScanQR
        }
    ]
})