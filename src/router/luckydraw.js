import Vue from 'vue'
import Router from 'vue-router'
import LuckyDraw from '@/components/luckydraw/index'
import StartDraw from '@/components/luckydraw/start'
import TapYourWristband from '@/components/luckydraw/tapYourWristband'
import CompleteDraw from '@/components/luckydraw/complete'
import Done from '@/components/luckydraw/done'
import ScanQR from '@/components/shared/QR/readQR'


Vue.use(Router)

export default new Router({
    routes: [{
            path: '/',
            name: 'LuckyDraw',
            component: LuckyDraw
        }, {
            path: '/luckydraw/start',
            name: 'StartDraw',
            component: StartDraw
        }, {
            path: '/luckydraw/tapyourwrisband',
            name: 'tapYourWristband',
            component: TapYourWristband
        }, {
            path: '/luckydraw/complete',
            name: 'CompleteDraw',
            component: CompleteDraw
        },
        {
            path: '/luckydraw/done',
            name: 'Done',
            component: Done
        },
        {
            path: '/scanqr',
            name: 'ScanQR',
            component: ScanQR
        }
    ]
})