import Vue from 'vue'
import App from './App.vue'
import env from 'dotenv'
import i18n from './language/index'
import VueTelInput from 'vue-tel-input'

/**
 * Createing Different Router to load multiple app
 * below Router and Store are for all modules inculded
 */
import router from './router'
import store from './store'
/**
 * 
 * Use below Router and Store to load Only Registration App
 */
// import router from './router/registration'
// import store from './store'
/**
 * Used below Router and Store to load Only Check-In App
 */
// import router from './router/checkin'
// import store from './store'
/**
 * Use below Router and Store to load Only Booth Activation App
 */
// import router from './router/booth'
// import store from './store'
/**
 * Use below Router and Store to load Only Lucky Draw App
 */
// import router from './router/luckydraw'
// import store from './store'
/**
 * Use below Router and Store to load Only REDEEM App
 */
// import router from './router/redeem'
// import store from './store'
/**
 * Use below Router and Store to load Only Check Out App
 */
// import router from './router/checkout'
// import store from './store'
/**
 * Use below Router and Store to load Only Q&A App
 */
// import router from './router/QA'
// import store from './store'

Vue.use(VueTelInput)

Vue.config.productionTip = false
env.config()

// Set True to activate QR Mode on App
let isQRCode = false
store.commit('updateisQRCode', isQRCode)

/**
 * Set Language in store
 */
let lang = 'en'
store.commit('setLanguage', lang)

// i18n.locale = 'th'

Vue.filter('toCurrency', function(value) {
    if (typeof value !== "number") {
        return value;
    }
    var formatter = new Intl.NumberFormat('th-TH', {
        // style: 'currency',
        // currency: 'THB',
        minimumFractionDigits: 0
    });
    return formatter.format(value);
});

new Vue({
    router,
    store: store,
    i18n: i18n,
    render: h => h(App)
}).$mount('#app')