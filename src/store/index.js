import Vue from 'vue'
import Vuex from 'vuex'
import i18n from '../language/index'

Vue.use(Vuex)

const store = new Vuex.Store({
    state: {
        landingInfo: {
            title: '',
            tapText: '',
            linkURL: '',
            showCancel: false,
            cancelURL: ''
        },
        userInfo: {
            firstName: '',
            lastName: '',
            email: '',
            phoneNumber: '',
            gender: '',
            jobTitle: '',
            company: '',
            nationality: '',
            dateOfBirth: '',
            country: '',
            inCome: ''
        },
        orderTotal: 0,
        selectedItems: [],
        orderBalance: 3000,
        orderServiceFee: 100,
        luckyNumber: '',
        isConfirm: false,
        isQRCode: false,
        beaconText: 'Scan your wristband',
        questionInfo: {},
        questionPoints: 0,
        questionIndex: 0,
        questionCount: 0,
        lang: 'en'
    },
    getters: {
        landingInfo: state => state.landingInfo,
        userInfo: state => state.userInfo,
        fullName: state => state.userInfo.firstName + " " + state.userInfo.lastName,
        orderTotal: state => state.orderTotal,
        orderSelectedItem: state => state.selectedItems,
        orderBalance: state => state.orderBalance,
        orderServiceFee: state => state.orderServiceFee,
        luckyNumber: state => state.luckyNumber,
        isConfirm: state => state.isConfirm,
        isQRCode: state => state.isQRCode,
        beaconText: state => state.beaconText,
        questionInfo: state => state.questionInfo,
        questionPoints: state => state.questionPoints,
        questionIndex: state => state.questionIndex,
        questionCount: state => state.questionCount,
        lang: state => state.lang
    },
    mutations: {
        updateLandingInfo: (state, landingInfoObj) => {
            state.landingInfo = landingInfoObj
        },
        addUserInfo: (state, userObject) => {
            state.userInfo = userObject
        },
        updateOrderTotal: (state, total) => {
            state.orderTotal = total
        },
        updateSelectedItem: (state, list) => {
            state.selectedItems = list
        },
        updateOrderBalance: (state, balance) => {
            state.orderBalance = balance
        },
        updateOrderServiceFee: (state, fee) => {
            state.orderServiceFee = fee
        },
        updateLuckyNumber: (state, number) => {
            state.luckyNumber = number
        },
        updateisConfirm: (state, confirm) => {
            state.isConfirm = confirm
        },
        updateisQRCode: (state, value) => {
            state.isQRCode = value
        },
        updateBeaconText: (state, value) => {
            state.beaconText = value
        },
        updateQuestionInfo: (state, value) => {
            state.questionInfo = value
        },
        updateQuestionPoints: (state, value) => {
            state.questionPoints += value
        },
        updateQuestionIndex: (state, value) => {
            state.questionIndex = value
        },
        updateQuestionCount: (state, value) => {
            state.questionCount = value
        },
        setLanguage: (state, value) => {
            state.lang = value
            i18n.locale = value
        }

    }
})

export default store