export default dataStorage => ({
    get(item){
        try {
            return JSON.parse(dataStorage.getItem(item))
        }
       catch(e){
           return null
       }
    },
    set(item,data){
        dataStorage.setItem(item,JSON.stringify(data))
    },
    remove(item) {
        dataStorage.remove(item)
    }
});